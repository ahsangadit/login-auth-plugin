<?php
/**
 * Plugin Name: Login Auth
 * Plugin URI:
 * Description: Provide authentication support in wordpress posts and pages.
 * Version: 1.0.0
 * Author: HZTech
 * Author URI: http://hztech.biz
 **/


if ( ! define( 'FOURCOUNTY_PLUGIN_VERSION', '1.0.0' ) ) {
	define( 'FOURCOUNTY_PLUGIN_VERSION', '1.0.0' );
}


if ( ! define( 'FOURCOUNTY_PLUGIN_DIR', plugin_dir_path( __FILE__ ) ) ) {
	define( 'FOURCOUNTY_PLUGIN_DIR', plugin_dir_path( __FILE__ ) );
}


if ( ! function_exists( 'wp_handle_upload' ) ) {
	require_once( ABSPATH . 'wp-admin/includes/file.php' );
}

if ( ! function_exists( 'fourCounty_plugin_script' ) ) {
	function fourCounty_plugin_script()
	{
		wp_enqueue_script( 'fourCounty-js', plugins_url( 'assets/js/fourCounty-script.js', __FILE__ ), array( 'jquery' ), rand( 0, 9999 ), true );
		wp_enqueue_style( 'fourCounty-css', plugins_url( 'assets/css/fourCounty-style.css', __FILE__ ) );
	}

	add_action( 'wp_enqueue_scripts', 'fourCounty_plugin_script' );

}

/* Register  Pages */
include( FOURCOUNTY_PLUGIN_DIR . 'inc/admin-pages/settingPage.php' );

/*Custom Meta Box For Posts*/
include( FOURCOUNTY_PLUGIN_DIR . 'inc/custom-fields/custom-meta-fields.php' );

add_filter( 'manage_page_posts_columns', 'smashing_CountyProtected_posts_columns' );
function smashing_CountyProtected_posts_columns( $columns )
{
	$columns['lock'] = __( 'Lock' );

	return $columns;
}

add_action( 'manage_page_posts_custom_column', 'smashing_CountyProtected_column', 10, 2 );
function smashing_CountyProtected_column( $column, $post_id )
{
	// Lock column

	if ( 'lock' === $column ) {
		$content = get_post_meta( $post_id, "countyProtected_", true );

		if ( ( $content == "restricted" ) ) {
			echo "Yes";
		} else {
			echo "No";
		}
	}

}

if ( ! class_exists( 'FourCountyAuthSupport' ) ) {
	include( FOURCOUNTY_PLUGIN_DIR . 'inc/functions.php' );
}
