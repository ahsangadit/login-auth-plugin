<div id="login-auth-form">
	<?php

	if ( ! isset( $_SESSION['county_protected'] ) ) {

		$logo       = get_option( 'fourCounty_setting_logo' );
		$url        = get_option( 'fourCounty_setting_url' );
		$plugin_err = "";

		if ( empty( $url ) ) {

			$plugin_err = "Plugin not configured, URL is missing!";
			?>
            <div class="err-section">
                <span>
                    <?= $plugin_err ?>
                </span>
            </div>
			<?php


		} else {
			?>

            <img src="<?php echo isset( $logo ) ? $logo : ''; ?>" id="logo-img"
                 style="width: 100px;"/>

			<?php
			if ( isset( $_SESSION['login_err'] ) ) {
				echo '<div class="custom-error"><strong>' . $_SESSION['login_err'] . '</strong></div>';
				unset( $_SESSION['login_err'] );
			}
			?>

            <form action="" method="post" name="4county-protected-form">
                <input type="hidden" name="action" value="4county-protected-login"/>
                <label for="">Username <strong>*</strong></label>
                <input type="text" name="username" value="">
                <label for="">Password <strong>*</strong></label>
                <input type="password" name="password" value="" maxlength="10">
                <br/>
                <div>
                    <input type="submit" name="submit" class="w-100" value="Login"/>
                </div>
            </form>

			<?php
		}

	} else {
		?>
        <div class="loginForm-4county">
            <form action="" method="post" name="4county-protected-form">
                <input type="hidden" name="action" value="4county-protected-logout"/>
                <div>
                    <input type="submit" name="submit_logout" value="Logout"/>
                </div>
            </form>
        </div>
		<?php
	}

	?>
</div>
