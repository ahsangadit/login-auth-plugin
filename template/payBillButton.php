<?php
$login_page = get_option( 'fourCounty_setting_loginPage' );
$url        = get_option( 'fourCounty_setting_url' );

?>
    <script type="text/javascript">
        function CallPortalValidation(event) {
            event.preventDefault();

			<?php if(isset( $_SESSION['county_protected'] )) { ?>

            var portalURL = "<?php echo $url; ?>/oscp/LoginValidate.aspx?feature=1"; // the full url to portal site.
            document.forms["payBillForm"].action = portalURL;
            document.forms["payBillForm"].method = "POST";
            document.forms["payBillForm"].target = "_blank";
            document.forms["payBillForm"].submit();

			<?php } else { ?>
            window.location.href = '<?= get_permalink( $login_page ) ?>';
			<?php } ?>
        }

        function ClearForm() {
            document.getElementById('textUserName').value = "";
            document.getElementById('textPassword').value = "";
        }
    </script>
<?php

$username   = "";
$password   = "";
$plugin_err = "";

if ( isset( $_SESSION['username'] ) && isset( $_SESSION['password'] ) ) {
	$username = $_SESSION['username'];
	$password = base64_decode( $_SESSION['password'] );
}

//Plugin not configured, missing URL

if ( empty( $url ) ) {
	$plugin_err = "Plugin not configured, URL is missing!";
	?>

    <div class="err-section">
            <span>
                <?= $plugin_err ?>
            </span>
    </div>

	<?php
} else {
	?>

    <div class="payBillForm-section">
        <form name="payBillForm" id="payBillForm" style="margin:0;padding:0;" action="" method="POST" target="_blank">
            <input type="hidden" name="textUserName" id="textUserName" value="<?= $username ?>" alt="Username">
            <input type="hidden" name="textPassword" id="textPassword" value="<?= $password ?>" alt="Password">
            <input type="hidden" name="action" value="payBill-action"/>
            <input type="submit" name="submit-payBill" id="login-auth-paybill-button" onclick="return CallPortalValidation(event)" value="Pay Bill">
            <!--<a id="myLink" href="javascript:CallPortalValidation();" alt="Login" title="Login" class="pay-Bill-Button">Pay Bill</a>-->
        </form>
    </div>

	<?php
}
