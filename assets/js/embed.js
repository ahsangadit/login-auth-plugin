var loginForm = '<form name="loginForm" id="embedded-login-auth-form" method="post" style="margin:0;padding:0;">\n' +
    '<div id="ajax-loginResponse"></div>' +
    '<input type="hidden" name="action" value="4county-protected-login"/>' +
    '<input value="testuser" placeholder="-- USERNAME --" name="username" type="text" class="textborder" id="login-auth-username" size="15" alt="Username" title="Username">\n' +
    '<input value="Web2020*" placeholder="-- PASSWORD --" name="password" type="password" class="textborder" id="login-auth-password" size="15" alt="Password" title="Password">\n' +
    '<input type="submit" id="embedded-login-auth-form-button" alt="Login" value="Login"><br>\n' +
    '</form>';

var elementExists = document.getElementById("login-auth-form");
elementExists.insertAdjacentHTML('afterend', loginForm);

var loginAuthCredentialsIsValid = false;
var loginAuthForm = document.getElementById("embedded-login-auth-form");
var loginAuthFormButton = document.getElementById("embedded-login-auth-form-button");

loginAuthForm.addEventListener("submit", function (e) {

    if (!loginAuthCredentialsIsValid)
        e.preventDefault();

    var username = document.getElementById('login-auth-username').value;
    var password = document.getElementById('login-auth-password').value;
    var data = new FormData();

    loginAuthFormButton.value = "Please wait...";
    loginAuthFormButton.disabled = true;

    data.append("textUserName", username);
    data.append("textPassword", password);

    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
            var res = JSON.parse(this.responseText);

            if (res.error) {
                setLoginAuthError(res.errorMessage);

                loginAuthFormButton.value = "Login";
                loginAuthFormButton.disabled = false;
            } else {
                loginAuthCredentialsIsValid = true;

                loginAuthForm.setAttribute('action', 'http://testsys.sitedevlink.com/login-auth/');
                loginAuthForm.submit();
            }
        }
    });

    xhr.open("POST", "https://testsys.sitedevlink.com/wp-admin/admin-ajax.php?action=auth_handler");

    xhr.send(data);

    return false;
});

function setLoginAuthError(error) {
    document.getElementById("ajax-loginResponse").innerText = error;
}
