<?php

/**
 * Register meta boxes.
 */
function hcf_register_meta_boxes()
{
	add_meta_box( 'hcf-1', __( 'Page Protected', 'hcf' ), 'hcf_display_callback_protectCheckbox_label', [
		'post',
		'page'
	], 'side' );
}

add_action( 'add_meta_boxes', 'hcf_register_meta_boxes' );

/**
 * Meta box display callback.
 *
 * @param WP_Post $post Current post object.
 */
function hcf_display_callback_protectCheckbox_label( $post )
{

	$content = get_post_meta( get_the_ID(), 'countyProtected_', true );

	?>
    <div class="hcf_box">
        <style scoped>
            .hcf_box {
                display: grid;
                grid-template-columns: max-content 1fr;
                grid-row-gap: 10px;
                grid-column-gap: 20px;
                margin: 10px 0;
            }

            .hcf_field {
                display: contents;
            }

        </style>
        <p class="meta-options hcf_field">
            <label for="hcf_author">Make this page protected</label>
            <input type="checkbox"
                   name="countyProtected_"
                   id="countyProtected_"
                   value="restricted"
				<?php if ( $content == "restricted" ) {
					echo esc_attr( "checked" );
				} else {
					echo esc_attr( null );
				} ?>
            />
        </p>
    </div>
	<?php
}


/*
 *Save meta box content.
 *
 * @param int $post_id Post ID
*/

function hcf_save_meta_box_countyProtect_checkbox( $post_id )
{
	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
		return;
	}

	if ( $parent_id = wp_is_post_revision( $post_id ) ) {
		$post_id = $parent_id;
	}

	if ( isset( $_POST['countyProtected_'] ) ) {
		update_post_meta( $post_id, 'countyProtected_', sanitize_text_field( $_POST['countyProtected_'] ) );
	} else {
		update_post_meta( $post_id, 'countyProtected_', sanitize_text_field( "unrestricted" ) );
	}

}

add_action( 'save_post', 'hcf_save_meta_box_countyProtect_checkbox' );
