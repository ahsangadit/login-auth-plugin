<?php


add_filter( 'manage_page_posts_columns', 'smashing_CountyProtected_posts_columns' );
function smashing_CountyProtected_posts_columns( $columns ) {
    $columns['lock'] = __( 'Lock' );
    return $columns;
}

add_action( 'manage_page_posts_custom_column', 'smashing_CountyProtected_column', 10, 2);
function smashing_CountyProtected_column( $column, $post_id ) {
    // Lock column

    if( 'lock' === $column ){
        $content = get_post_meta($post_id , "countyProtected_" ,true);

        if(( $content == "restricted" )){
            echo "Yes";
        }else{
            echo "No";
        }
    }

}



