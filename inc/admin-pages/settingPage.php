<?php


function fourCountyProtected_register_menu_page()
{
	add_menu_page( 'Login Auth', 'Login Auth', 'manage_options', 'login-auth-settings', 'fourCounty_settings_page_html', 'dashicons-post-status', 30 );
}

add_action( 'admin_menu', 'fourCountyProtected_register_menu_page' );


function fourCounty_plugin_settings()
{

	register_setting( 'fourCounty-settings', 'fourCounty_setting_logo', 'handle_file_upload' );
	register_setting( 'fourCounty-settings', 'fourCounty_setting_loginRedirect' );
	register_setting( 'fourCounty-settings', 'fourCounty_setting_loginPage' );
	register_setting( 'fourCounty-settings', 'fourCounty_setting_url' );


	//register_setting('fourCounty-settings', 'fourCounty_setting_protectPages' , 'handle_multiSelect');


	add_settings_section( 'fourCounty_label_setting_section', 'Login Auth Settings', 'fourCounty_plugin_setting_section_callback', 'fourCounty-settings' );


	add_settings_field( 'fourCounty_logo_field', 'Your Logo', 'fourCounty_plugin_setting_logo_field_callback', 'fourCounty-settings'
		, 'fourCounty_label_setting_section'
	);

	add_settings_field( 'fourCounty_loginRedirect_field', 'Login Redirect', 'fourCounty_plugin_setting_loginRedirect_field_callback', 'fourCounty-settings'
		, 'fourCounty_label_setting_section'
	);

	add_settings_field( 'fourCounty_loginPage_field', 'Login Page', 'fourCounty_plugin_setting_loginPage_field_callback', 'fourCounty-settings'
		, 'fourCounty_label_setting_section'
	);

	add_settings_field( 'fourCounty_url_field', 'URL', 'fourCounty_plugin_setting_url_field_callback', 'fourCounty-settings'
		, 'fourCounty_label_setting_section'
	);

	//    add_settings_field('fourCounty_protectPages_field', 'Protect Pages', 'fourCounty_plugin_setting_protectPages_field_callback', 'fourCounty-settings'
	//        , 'fourCounty_label_setting_section'
	//    );

}

add_action( 'admin_init', 'fourCounty_plugin_settings' );

function fourCounty_plugin_setting_section_callback()
{
	//echo '<p>Login Auth Settings</p>';
}

function fourCounty_plugin_setting_logo_field_callback()
{
	$logo = get_option( 'fourCounty_setting_logo' );
	?>
    <div><input type="text" placeholder="Logo Image URL" name="logo_url" value="<?= $logo ?>" style="width: 300px;"/></div>
    <img src="<?php echo isset( $logo ) ? $logo : ''; ?>" id="logo-img"
         style="width: 100px;"/>

    <br>
    <input type="file" name="fourCounty_setting_logo"
           value="<?php echo isset( $logo ) ? esc_attr( $logo ) : ''; ?>"
           class="custom-file-input" name="logo"
           id="fourCounty_setting_logo"
    >
    <script>
        jQuery(function ($) {

            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        $('#logo-img').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]);
                }
            }

            $("#fourCounty_setting_logo").change(function () {
                readURL(this);
            });

        });
    </script>
	<?php
}

function handle_file_upload( $option )
{

	if ( ! empty( $_FILES["fourCounty_setting_logo"]["tmp_name"] ) ) {
		$urls = wp_handle_upload( $_FILES["fourCounty_setting_logo"], array( 'test_form' => false ) );
		$temp = $urls["url"];

		return $temp;
	} else {

		//$logoURL = get_option( 'fourCounty_setting_logo' );
		$logoURL = $_POST['logo_url'];

		return $logoURL;
	}

	return $option;
}


function fourCounty_plugin_setting_loginRedirect_field_callback()
{
	// get the value of the setting we've registered with register_setting()
	$selected_page = get_option( 'fourCounty_setting_loginRedirect' );
	// output the field

	$pages = get_posts( array(
		                    'post_type'      => 'page',
		                    'posts_per_page' => - 1
	                    ) );

	?>

    <select name="fourCounty_setting_loginRedirect" id="fourCounty_setting_loginRedirect" style="width: 300px;">
        <option value="">Select Page</option>
		<?php foreach ( $pages as $key => $page ): ?>
            <option value="<?= $page->ID ?>" <?php if ( $page->ID == $selected_page ) { ?> selected <?php } else {
				null;
			} ?> > <?= $page->post_title ?> </option>
		<?php endforeach; ?>
    </select>

	<?php
}

function fourCounty_plugin_setting_loginPage_field_callback()
{

	$selected_login_page = get_option( 'fourCounty_setting_loginPage' );

	$pages = get_posts( array(
		                    'post_type'      => 'page',
		                    'posts_per_page' => - 1
	                    ) );

	?>

    <select name="fourCounty_setting_loginPage" id="fourCounty_setting_loginPage" style="width: 300px;">
        <option value="">Select Page</option>
		<?php foreach ( $pages as $key => $page ): ?>
            <option value="<?= $page->ID ?>" <?php if ( $page->ID == $selected_login_page ) { ?> selected <?php } else {
				null;
			} ?> > <?= $page->post_title ?> </option>
		<?php endforeach; ?>
    </select>

	<?php

}

function fourCounty_plugin_setting_protectPages_field_callback()
{

	$pageIDs = get_option( 'fourCounty_setting_protectPages' );

	$pages = get_posts( array(
		                    'post_type'      => 'page',
		                    'posts_per_page' => - 1
	                    ) );

	?>

    <select name="fourCounty_setting_protectPages[]" id="fourCounty_setting_protectPages" multiple
            style="width: 300px;">
		<?php foreach ( $pages as $key => $page ): ?>
            <option value="<?= $page->ID ?>" <?php if ( in_array( $page->ID, $pageIDs ) ) { ?> selected <?php } else {
				null;
			} ?>> <?= $page->post_title ?> </option>
		<?php endforeach; ?>
    </select>

	<?php
}

function handle_multiSelect( $option )
{

	$restrictedPageIds = $_POST['fourCounty_setting_protectPages'];

	$IDS = get_posts( array(
		                  'field'          => 'ids',
		                  'post_type'      => 'page',
		                  'posts_per_page' => - 1
	                  ) );

	$pageIDs = [];

	foreach ( $IDS as $key => $post ) {
		$pageIDs [] = $post->ID;
	}

	foreach ( $restrictedPageIds as $key => $val ) {

		if ( in_array( $val, $pageIDs ) ) {
			update_post_meta( $val, "countyProtected", "restricted" );
		}
	}


	$unMatchIDS = array_diff( $pageIDs, $restrictedPageIds );

	foreach ( $unMatchIDS as $key => $val ) {
		update_post_meta( $val, "countyProtected", "unrestricted" );
	}


	return $option;

}


function fourCounty_plugin_setting_url_field_callback()
{

	// get the value of the setting we've registered with register_setting()
	$url = get_option( 'fourCounty_setting_url' );
	?>
    <input type="text" name="fourCounty_setting_url" id="fourCounty_setting_url"
           value="<?php echo isset( $url ) ? esc_attr( $url ) : null; ?>" style="width: 300px;">
	<?php
}


function fourCounty_settings_page_html()
{

	if ( ! is_admin() ) {
		return;
	}
	?>
    <div class="wrap">
        <form action="options.php" method="post" enctype="multipart/form-data">
			<?php
			settings_fields( 'fourCounty-settings' );
			do_settings_sections( 'fourCounty-settings' );
			submit_button( 'Save Changes' );
			?>
        </form>
    </div>
	<?php
}







