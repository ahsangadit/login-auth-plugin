<?php

class FourCountyAuthSupport
{

	public function __construct()
	{
		session_start();
		add_shortcode( 'login-form', array( $this, 'custom_login_form' ) );
		add_shortcode( 'paybill-button', array( $this, 'custom_payBill_form' ) );
		// add_action( 'wp_enqueue_scripts', array( $this, 'enqueue_scripts' ) );
		add_action( 'template_redirect', array( $this, 'my_callback' ) );
		add_action( 'wp_loaded', array( $this, 'customFormProcess' ) );
		add_action( 'wp_ajax_auth_handler', array( $this, 'embed_auth_handler' ) );
		add_action( 'wp_ajax_nopriv_auth_handler', array( $this, 'embed_auth_handler' ) );
		add_action( 'wp', array( $this, 'login_auth_embed_script' ) );
	}

	public function login_auth_embed_script()
	{
		if ( isset( $_GET['login_embed'] ) ) {
			header( 'Content-type: text/javascript' );

			echo file_get_contents( dirname( __FILE__ ) . '/../assets/js/embed.js' );
			die;
		}
	}

	public function embed_auth_handler()
	{
		header( "Access-Control-Allow-Origin: *" );

		$username = $_POST['textUserName'];
		$password = $_POST['textPassword'];

		$status = $this->auth4County( $username, $password );

		echo json_encode( [
			                  'error'        => ! $status,
			                  'errorMessage' => "Username/Password not found"
		                  ] );

		die;
	}

	public function enqueue_scripts()
	{
		wp_enqueue_script( 'four-county-auth-support', plugins_url( 'assets/js/script.js', __FILE__ ), array( 'jquery' ), rand( 0, 9999 ), true );
		wp_enqueue_style( 'four-county-auth-support', plugins_url( 'assets/css/style.css', __FILE__ ) );
	}

	function my_callback()
	{

		global $post;
		//$session         = isset($_SESSION['county_protected']) ? $_SESSION['county_protected'] : array();
		//$data            = explode("_",$session);

		$login_page = get_option( 'fourCounty_setting_loginPage' );
		$page       = get_post( $login_page );

		if ( ! is_page( $page->post_name ) ) {

			$countyProtected = get_post_meta( $post->ID, 'countyProtected_', true );
			$has_session     = isset( $_SESSION['county_protected'] );
			$can_view        = $countyProtected !== 'restricted' || ( $countyProtected === 'restricted' && $has_session );

			if ( current_user_can( 'manage_options' ) ) {
				$can_view = true;
			}

			if ( is_home() ) {
				$blog_page      = get_option( 'page_for_posts' );
				$blog_protected = get_post_meta( $blog_page, 'countyProtected_', true );

				if ( $blog_protected === 'restricted' && ! $has_session ) {
					$can_view = false;
				}
			}

			if ( ! $can_view ) {
				$login_page = get_option( 'fourCounty_setting_loginPage' );
				wp_redirect( get_permalink( $login_page ) );
				exit;
			}
		}
	}

	function custom_login_form()
	{
		ob_start();
		include FOURCOUNTY_PLUGIN_DIR . '/template/loginForm.php';

		return ob_get_clean();
	}


	function customFormProcess()
	{

		//For Custom Login Form Process

		if ( $_POST['action'] == '4county-protected-login' ) {
			$this->login_form_valid( $_POST['username'], $_POST['password'] );
		}

		if ( isset( $_POST['submit_logout'] ) && $_POST['action'] == '4county-protected-logout' ) {

			unset( $_SESSION['county_protected'] );
			unset( $_SESSION['username'] );
			unset( $_SESSION['password'] );

			$login_page = get_option( 'fourCounty_setting_loginPage' );
			wp_redirect( get_permalink( $login_page ) );

		}

	}


	function login_form_valid( $username, $password )
	{

		global $customize_error_validation;
		$customize_error_validation = new WP_Error;

		$status = $this->auth4County( $username, $password );

		if ( $status != true ) {
			$customize_error_validation->add( 'field', 'Username & Password not found!' );

			if ( is_wp_error( $customize_error_validation ) ) {
				foreach ( $customize_error_validation->get_error_messages() as $error ) {
					//echo  '<div class="custom-error"><strong>Hold</strong>:' . $error . '</div>';
					$_SESSION['login_err'] = $error;
				}
			}

		} else {

			$pageID = get_option( 'fourCounty_setting_loginRedirect' );

			$_SESSION['county_protected'] = "protected_" . $username;
			$_SESSION['username']         = $username;
			$_SESSION['password']         = base64_encode( $password );

			if ( isset( $pageID ) ) {

				$_SESSION['login_err'] = "";

				wp_redirect( get_permalink( $pageID ) );
				exit;
			}

		}


	}

	function auth4County( $username, $password )
	{
		$curl = curl_init();

		$url = get_option( 'fourCounty_setting_url' );

		curl_setopt_array( $curl, array(
			CURLOPT_URL            => $url . "/oscp/LoginValidate.aspx?feature=1",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING       => "",
			CURLOPT_MAXREDIRS      => 10,
			CURLOPT_TIMEOUT        => 0,
			CURLOPT_FOLLOWLOCATION => true,
			CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST  => "POST",
			CURLOPT_POSTFIELDS     => array( 'textUserName' => $username, 'textPassword' => $password ),
		) );

		$response = curl_exec( $curl );

		curl_close( $curl );
		$found = strrpos( $response, 'Payments/MakePayment/PaymentAccountList' );

		return $found !== false;
	}


	function custom_payBill_form()
	{
		ob_start();
		include FOURCOUNTY_PLUGIN_DIR . '/template/payBillButton.php';

		return ob_get_clean();
	}

}

new FourCountyAuthSupport();
